# Template MVVM project

Template project for the MVVM architecture implementation.

1. Download ZIP project or Clone and update the Git root path via AndroidStudio.
2. Refactor the "appname" package to the actual name of the application.
3. Search for any other "appname" occurrences in the project.