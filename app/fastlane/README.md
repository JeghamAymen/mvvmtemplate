fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Runs all the tests
### android crashlytics_staging
```
fastlane android crashlytics_staging
```
Submit a new staging Build to Crashlytics Beta
### android crashlytics_original
```
fastlane android crashlytics_original
```
Submit a new production Build to Crashlytics Beta
### android deploy_firebase_original
```
fastlane android deploy_firebase_original
```
Submit a new production Build to Firebase App Distribution
### android deploy_firebase_staging
```
fastlane android deploy_firebase_staging
```
Submit a new staging Build to Firebase App Distribution
### android promote_production
```
fastlane android promote_production
```
Promote the last version to the production track
### android deploy_beta
```
fastlane android deploy_beta
```
Deploy a new version to the Google Play beta test track
### android deploy_production
```
fastlane android deploy_production
```
Deploy a new version to the Google Play

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
