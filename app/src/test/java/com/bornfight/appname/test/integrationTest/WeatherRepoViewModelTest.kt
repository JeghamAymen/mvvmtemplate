package com.bornfight.appname.test.integrationTest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bornfight.appname.test.data.TestData
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.common.scheduler.TestSchedulerProvider
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class WeatherRepoViewModelTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    lateinit var schedulers: SchedulerProvider

    /*
    @Mock
    lateinit var weatherRepository: WeatherRepo
    lateinit var viewModel: WeatherViewModel
     */

    val testData = TestData()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        schedulers = TestSchedulerProvider()
        // viewModel = WeatherViewModel(schedulers, weatherRepository)
    }

    @Test
    fun initTest() {
        assert(true)
    }

    /*
    @Test
    fun testNull() {
        assertThat(viewModel, notNullValue())
        assertThat(viewModel.forecastData, notNullValue())
        assertThat(viewModel.weatherData, notNullValue())
    }

    @Test
    fun testWeatherGet() {
        `when`(weatherRepository.getCurrentWeatherForCity("Zagreb")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getWeather(), null))
        )

        viewModel.getCurrentWeatherData("Zagreb")

        verify(weatherRepository).getCurrentWeatherForCity("Zagreb")
        verifyNoMoreInteractions(weatherRepository)
    }

    @Test
    fun testForecastGet() {
        val date = System.currentTimeMillis()

        `when`(weatherRepository.getForecastForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getForecastData(), null))
        )

        viewModel.getForecastData("London", date)

        verify(weatherRepository).getForecastForCity("London")
        verifyNoMoreInteractions(weatherRepository)
    }
     */

}