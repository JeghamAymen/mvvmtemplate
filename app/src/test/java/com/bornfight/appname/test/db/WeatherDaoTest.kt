package com.bornfight.appname.test.db

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bornfight.appname.test.data.TestData
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P]) // TODO remove when RoboElectric compatible with Android Q
class WeatherDaoTest : DbTest() {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    val data = TestData()

    @Test
    fun initTest() {
        assert(true)
    }

    /*
    @Test
    fun insertWeatherResponseAndRead() {
        val weatherResponse = data.getWeather()

        AsyncTask.execute {
            db.weatherDao().saveWeatherData(weatherResponse)
        }

        Thread.sleep(1000)
        
        AsyncTask.execute {
            val loaded = db.weatherDao().getWeatherData("London").blockingFirst()

            assertThat(loaded, notNullValue())
            assertThat(loaded?.name, `is`("London"))
        }
    }

     */


}