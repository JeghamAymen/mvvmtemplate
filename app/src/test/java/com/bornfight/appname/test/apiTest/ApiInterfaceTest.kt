package com.bornfight.appname.test.apiTest

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bornfight.appname.testDemo.data.TestData
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.data.retrofit.RxErrorHandlingCallAdapterFactory
import com.bornfight.utils.GsonUtil
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P]) // TODO remove when RoboElectric compatible with Android Q
class ApiInterfaceTest {

    private val data = TestData()

    /*
    private val weatherResponse = data.getWeather()

    private val forecastResponse = data.getForecastJSonTest()

     */

    @Rule
    @JvmField
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var mockWebServer: MockWebServer

    lateinit var apiInterface: ApiInterface

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()

        val gson = GsonUtil.defaultGson
        apiInterface = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(ApiInterface::class.java)
    }

    @After
    fun cleanUp() {
        mockWebServer.shutdown()
    }

    @Test
    fun initTest() {
        assert(true)
    }
    /*
    @Test
    fun getWeather() {
        enqueResponse("{\n  \"coord\": {\n    \"lon\": -0.13,\n    \"lat\": 51.51\n  },\n  \"weather\": [\n    {\n      \"id\": 803,\n      \"main\": \"Clouds\",\n      \"description\": \"broken clouds\",\n      \"icon\": \"04d\"\n    }\n  ],\n  \"base\": \"stations\",\n  \"main\": {\n    \"temp\": 280.5,\n    \"pressure\": 1008,\n    \"humidity\": 75,\n    \"temp_min\": 279.15,\n    \"temp_max\": 282.04\n  },\n  \"visibility\": 10000,\n  \"wind\": {\n    \"speed\": 4.1,\n    \"deg\": 330\n  },\n  \"rain\": {\n  },\n  \"clouds\": {\n    \"all\": 75\n  },\n  \"dt\": 1573228450,\n  \"sys\": {\n    \"type\": 1,\n    \"id\": 1502,\n    \"country\": \"GB\",\n    \"sunrise\": 1573196741,\n    \"sunset\": 1573230170\n  },\n  \"timezone\": 0,\n  \"id\": 2643743,\n  \"name\": \"London\",\n  \"cod\": 200\n}")
        val data = apiInterface.getWeatherForCity("London").blockingFirst()

        assertThat(data, notNullValue())
        assertEquals(weatherResponse, data)

    }
     */

    private fun enqueResponse(file: String) {
        mockWebServer.enqueue(MockResponse().setBody(getJson(file)))
    }

    private fun getJson(path: String): String {
        val uri = this.javaClass.classLoader?.getResource(path)
        val file = File(uri?.path)
        return String(file.readBytes())
    }

}