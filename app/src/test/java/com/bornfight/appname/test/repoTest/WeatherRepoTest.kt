package com.bornfight.appname.test.repoTest

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P]) // TODO remove when RoboElectric compatible with Android Q
class WeatherRepoTest {

    val testScheduler = TestScheduler()

    /*
    @Mock
    lateinit var weatherRepo: WeatherRepo
    val testData: TestData = TestData()
     */

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun initTest() {
        assert(true)
    }

    /*
    @Test
    fun getWeatherResponse_Success() {
        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getWeather(), null))
        )

        val testObserver = TestObserver<ResourceState<WeatherResponse>>()
        weatherRepo.getCurrentWeatherForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().data

        assertEquals(testData.getWeather(), result)
    }

    @Test
    fun getWeatherResponse_Error() {
        val error = BaseError("Error went down")

        Mockito.`when`(weatherRepo.getCurrentWeatherForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.ERROR, null, error))
        )

        val testObserver = TestObserver<ResourceState<WeatherResponse>>()
        weatherRepo.getCurrentWeatherForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().error

        assertEquals(error, result)
    }

    @Test
    fun getForecastResponse_Success() {
        Mockito.`when`(weatherRepo.getForecastForCity("Zagreb")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getForecastData(), null))
        )

        val testObserver = TestObserver<ResourceState<List<Forecast>>>()
        weatherRepo.getForecastForCity("Zagreb").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().data

        assertEquals(testData.getForecastData(), result)
    }

    @Test
    fun getForecastResponse_Error() {
        val error = BaseError("Error went down")

        Mockito.`when`(weatherRepo.getForecastForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.ERROR, null, error))
        )

        val testObserver = TestObserver<ResourceState<List<Forecast>>>()
        weatherRepo.getForecastForCity("London").subscribe(testObserver)

        testScheduler.triggerActions()

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val result = testObserver.values().first().error

        assertEquals(error, result)
    }
     */

}