package com.bornfight.appname.testDemo.integrationTest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bornfight.appname.testDemo.data.TestData
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.Status
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.common.scheduler.TestSchedulerProvider
import com.bornfight.demo.weather.repository.weather.WeatherRepo
import com.bornfight.demo.weather.weather.WeatherViewModel
import io.reactivex.Observable
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.notNullValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class WeatherRepoViewModelTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var weatherRepository: WeatherRepo
    lateinit var schedulers: SchedulerProvider

    lateinit var viewModel: WeatherViewModel
    val testData = TestData()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        schedulers = TestSchedulerProvider()
        viewModel = WeatherViewModel(schedulers, weatherRepository)
    }

    @Test
    fun testNull() {
        assertThat(viewModel, notNullValue())
        assertThat(viewModel.forecastData, notNullValue())
        assertThat(viewModel.weatherData, notNullValue())
    }

    @Test
    fun testWeatherGet() {
        `when`(weatherRepository.getCurrentWeatherForCity("Zagreb")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getWeather(), null))
        )

        viewModel.getCurrentWeatherData("Zagreb")

        verify(weatherRepository).getCurrentWeatherForCity("Zagreb")
        verifyNoMoreInteractions(weatherRepository)
    }

    @Test
    fun testForecastGet() {
        val date = System.currentTimeMillis()

        `when`(weatherRepository.getForecastForCity("London")).thenReturn(
            Observable.just(ResourceState(Status.SUCCESS, testData.getForecastData(), null))
        )

        viewModel.getForecastData("London", date)

        verify(weatherRepository).getForecastForCity("London")
        verifyNoMoreInteractions(weatherRepository)
    }

}