package com.bornfight.appname.testDemo.db

import android.os.AsyncTask
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bornfight.appname.test.db.DbTest
import com.bornfight.appname.testDemo.data.TestData
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

// TODO remove Config annotation when Roboelectric fixes Android Q compatibility
@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class WeatherDaoTest : DbTest() {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    val data = TestData()

    @Test
    fun insertWeatherResponseAndRead() {
        val weatherResponse = data.getWeather()

        AsyncTask.execute {
            db.weatherDao().saveWeatherData(weatherResponse)
        }

        Thread.sleep(1000)

        AsyncTask.execute {
            val loaded = db.weatherDao().getWeatherData("London").blockingFirst()

            assertThat(loaded, notNullValue())
            assertThat(loaded?.name, `is`("London"))
        }
    }

    @Test
    fun insertForecastDataAndRead() {
        val forecastResponse = data.getForecastData()

        AsyncTask.execute {
            db.weatherDao().saveForecastData(forecastResponse)
        }

        Thread.sleep(1000)

        AsyncTask.execute {
            val loaded = db.weatherDao().getForecastData("Zagreb").blockingFirst()

            assertThat(loaded, notNullValue())
            assertThat(loaded?.size, `is`(6))
        }
    }

}