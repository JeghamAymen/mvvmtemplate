package com.bornfight.appname.testDemo.apiTest

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bornfight.appname.testDemo.data.TestData
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.data.retrofit.RxErrorHandlingCallAdapterFactory
import com.bornfight.utils.GsonUtil
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P]) // TODO remove when RoboElectric compatible with Android Q
class ApiInterfaceTest {

    private val data = TestData()

    private val weatherResponse = data.getWeather()

    private val forecastResponse = data.getForecastJSonTest()

    @Rule
    @JvmField
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var mockWebServer: MockWebServer

    lateinit var apiInterface: ApiInterface

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()

        val gson = GsonUtil.defaultGson
        apiInterface = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(ApiInterface::class.java)
    }

    @After
    fun cleanUp() {
        mockWebServer.shutdown()
    }

    @Test
    fun getWeather() {
        enqueResponse("test/getWeather.json")
        val data = apiInterface.getWeatherForCity("London").blockingFirst()

        assertThat(data, notNullValue())
        assertEquals(weatherResponse, data)

    }

    @Test
    fun getForecast() {
        enqueResponse("test/getForecast.json")

        val data = apiInterface.getForecastForCity("London").blockingFirst()

        assertThat(data, notNullValue())
        assertEquals(forecastResponse, data)
    }

    private fun enqueResponse(file: String) {
        mockWebServer.enqueue(MockResponse().setBody(getJson(file)))
    }

    private fun getJson(path: String): String {
        val uri = this.javaClass.classLoader?.getResource(path)
        val file = File(uri?.path)
        return String(file.readBytes())
    }

}