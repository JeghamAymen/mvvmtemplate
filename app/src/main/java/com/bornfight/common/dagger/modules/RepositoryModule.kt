package com.bornfight.common.dagger.modules

import com.bornfight.demo.weather.repository.news.NewsRepo
import com.bornfight.demo.weather.repository.news.NewsRepoImpl
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionRepo
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionRepoImpl
import com.bornfight.demo.weather.repository.weather.WeatherRepo
import com.bornfight.demo.weather.repository.weather.WeatherRepoImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesSearchSuggestionRepo(searchSuggestionRepoImpl: SearchSuggestionRepoImpl): SearchSuggestionRepo =
        searchSuggestionRepoImpl

    @Provides
    @Singleton
    fun providesCurrentWeatherRepo(weatherRepoImpl: WeatherRepoImpl): WeatherRepo = weatherRepoImpl

    @Provides
    @Singleton
    fun provideNewsRepo(newsRepoImpl: NewsRepoImpl): NewsRepo = newsRepoImpl

}