package com.bornfight.common.dagger.components

import android.content.Context
import com.bornfight.appname.App
import com.bornfight.common.dagger.modules.*
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.demo.weather.repository.mock.MockRepo
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by lleopoldovic on 13/09/2019.
 */

@Singleton
@Component(
    modules = [
        (AndroidInjectionModule::class),

        (BindingModuleActivities::class),
        (BindingModuleFragments::class),
        (BindingModuleServices::class),

        (SchedulersModule::class),

        (ApiServiceModule::class),
        (DatabaseModule::class),
        (ViewModelModule::class),
        (RepositoryModule::class)
    ]
)
interface ApplicationComponent : AndroidInjector<App> {

    val context: Context

    val apiInterface: ApiInterface

    val database: AppDatabase

    val sessionPrefImpl: SessionPrefImpl

    val devicePrefs: DevicePreferences

    // Add repos here:
    val mockRepo: MockRepo
}