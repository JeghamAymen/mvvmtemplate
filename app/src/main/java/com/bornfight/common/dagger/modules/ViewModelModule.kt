package com.bornfight.common.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bornfight.common.dagger.qualifiers.ViewModelKey
import com.bornfight.common.mvvm.ViewModelFactory
import com.bornfight.demo.weather.news.NewsViewModel
import com.bornfight.demo.weather.searchsuggestions.SearchSuggestionsViewModel
import com.bornfight.demo.weather.weather.WeatherViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by lleopoldovic on 16/09/2019.
 */

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    // Add every new ViewModel here, following the IntroViewModel example:
    @Binds
    @IntoMap
    @ViewModelKey(SearchSuggestionsViewModel::class)
    abstract fun bindIntroViewModel(searchSuggestionsViewModel: SearchSuggestionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WeatherViewModel::class)
    abstract fun bindCurrentWeatherViewModel(weatherViewModel: WeatherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewsViewModel::class)
    abstract fun bindNewsViewModel(newsViewModel: NewsViewModel): ViewModel

}