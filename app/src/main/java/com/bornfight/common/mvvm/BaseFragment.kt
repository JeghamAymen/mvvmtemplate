package com.bornfight.common.mvvm

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by lleopoldovic on 16/09/2019.
 */

abstract class BaseFragment : Fragment(), BaseView, HasAndroidInjector {

    // DI:
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    // UI vars
    //Added a check, tests dont pass if there is no check(EmptyFragmentActivity cant be cast to BaseActivity)
    val baseActivity: BaseActivity? by lazy {
        if (activity is BaseActivity) {
            activity as BaseActivity
        } else {
            null
        }
    }


    /*
        Lifecycle
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        // Fragments use AndroidSupportInjection
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////


    /*
        UI funcs:
     */
    override fun showError(errorMessage: String) {
        baseActivity?.showShortInfo(errorMessage)
    }

    override fun showError(stringResourceId: Int) {
        baseActivity?.showError(stringResourceId)
    }

    override fun showShortInfo(info: String) {
        baseActivity?.showShortInfo(info)
    }

    override fun showShortInfo(stringResourceId: Int) {
        baseActivity?.showShortInfo(stringResourceId)
    }

    override fun showProgressCircle(show: Boolean) {
        baseActivity?.showProgressCircle(show)
    }

    override fun showLoader(show: Boolean) {
        baseActivity?.showLoader(show)
    }

    override fun showLoader(show: Boolean, cancelable: Boolean) {
        baseActivity?.showLoader(show, cancelable)
    }

    override fun showInfoDialog(title: String?, description: String?, buttonText: String?) {
        baseActivity?.showInfoDialog(title, description, buttonText)
    }

    override fun showInfoDialog(@StringRes titleResourceId: Int, @StringRes descriptionResourceId: Int, @StringRes buttonText: Int) {
        baseActivity?.showInfoDialog(titleResourceId, descriptionResourceId, buttonText)
    }

    override fun hideKeyboard(view: View) {
        baseActivity?.hideKeyboard(view)
    }

    override fun onLogout() {
        baseActivity?.onLogout()
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    /*
        Permissions funcs:
     */
    protected fun checkPermissionGranted(permission: String): Boolean {
        return baseActivity?.checkPermissionGranted(permission) == true
    }

    protected fun checkPermissionsGranted(vararg permissions: String): Boolean {
        return baseActivity?.checkPermissionsGranted(*permissions) == true
    }

    protected fun checkPermissionDeniedForGood(permission: String): Boolean {
        return baseActivity?.checkPermissionDeniedForGood(permission) == true
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
}