package com.bornfight.common.mvvm.pagination

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.bornfight.common.mvvm.BaseViewModel
import com.bornfight.common.mvvm.basemodels.BaseError
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.Status
import com.bornfight.common.scheduler.SchedulerProvider
import io.reactivex.Observable

/**
 * Created by lleopoldovic on 28/01/2019.
 *
 * Extend this class if your ViewModel WILL implement pagination via [GenericPaginationDataSourceFactory].
 * Otherwise, use [BaseViewModel].
 */

abstract class BasePaginationViewModel(schedulers: SchedulerProvider) : BaseViewModel(schedulers) {

    /**
     * Use this call to build LivePagedList, instead of [LivePagedListBuilder]. This method will wrap the return type
     * of {@link LivePagedListBuilder#build()} with [ResourceState] so you only have to observe the live data which is
     * returned by this call to be able to submit the PagedData to your pagination adapter and also show loading/error
     * states of your database/API call. Make sure to use [ResourceStateObserver] in order to observe the results.
     */
    fun <DataType, PaginationParams> buildLivePagedListWithState(
        dataSourceFactory: GenericPaginationDataSourceFactory<DataType, PaginationParams>,
        paginationConfig: PagedList.Config
    ): LiveData<ResourceState<PagedList<DataType>>> {
        // Input values
        val pagedDataList =
            LivePagedListBuilder<Int, DataType>(dataSourceFactory, paginationConfig).build()
        var fetchState: MutableLiveData<ResourceState<DataType>> = dataSourceFactory.getFetchState()
        // Output value
        val mergedSource = MediatorLiveData<ResourceState<PagedList<DataType>>>()

        mergedSource.addSource(pagedDataList) { pagedList ->
            (pagedList?.dataSource as? GenericPaginationDataSource<DataType, PaginationParams>)?.let {
                // Every time dataSource.invalidate() is called, a new PagedList<DataType> is created. Therefore, a new
                // instance of fetchState within a newly created instance of GenericPaginationDataSource will be created.
                // Hence, the previous instance has to be removed from the merged source and the new one has to be added.
                mergedSource.removeSource(fetchState)
                fetchState = it.getFetchState()

                mergedSource.addSource(fetchState) { newFetchState ->
                    setMergedSourceDataByFetchState(newFetchState, pagedDataList, mergedSource)
                }
            }

            // Now, with the newest and current instance of fetchState, it is possible to properly wrap and propagate
            // the results of the pagination to its observers.
            when (fetchState.value?.status) {
                Status.SUCCESS -> mergedSource.value = ResourceState.success(pagedList)
                Status.LOADING -> mergedSource.value = ResourceState.loading(pagedList)
                Status.ERROR -> mergedSource.value = ResourceState.error(
                    data = pagedList,
                    baseError = fetchState.value?.error ?: BaseError(
                        errorMessage = BaseError.baseErrorMessage
                    )
                )
            }
        }
        return mergedSource
    }

    /**
     * Use this method to generate an instance of [GenericPaginationDataSourceFactory].
     */
    fun <DataType, PaginationParams> generatePaginationDataSourceFactory(
        paginationParams: GenericPaginationParams<PaginationParams>,
        call: (params: GenericPaginationParams<PaginationParams>) -> Observable<ResourceState<List<DataType>>>
    ): GenericPaginationDataSourceFactory<DataType, PaginationParams> {
        return GenericPaginationDataSourceFactory(
            params = paginationParams,
            call = call,
            compositeDisposable = compositeDisposable
        )
    }

    /**
     * Use this method to generate the default configuration of a PagedList.
     *
     * In most cases, the default PagedList configuration will work since the total count of items is usually not
     * available in remote APIs so placeholders should be disabled and our ih-house practises have not set a larger
     * initial load size hint than @param paginationParams.pageSize * 2.
     */
    fun <PaginationParams> generateDefaultPaginationConfig(
        paginationParams: GenericPaginationParams<PaginationParams>
    ): PagedList.Config {
        return PagedList.Config.Builder()
            .setPageSize(paginationParams.pageSize)
            .setInitialLoadSizeHint(paginationParams.pageSize * 2) // If not set, initialLoadSizeHint equals PAGE_SIZE * 3
            .setEnablePlaceholders(false) // Since the total count is usually not available in remote APIs, placeholders should be disabled.
            .build()
    }


    private fun <DataType> setMergedSourceDataByFetchState(
        fetchState: ResourceState<DataType>,
        pagedDataList: LiveData<PagedList<DataType>>,
        mergedSource: MediatorLiveData<ResourceState<PagedList<DataType>>>
    ) {
        when (fetchState.status) {
            Status.SUCCESS -> mergedSource.value = ResourceState.success(pagedDataList.value)
            Status.LOADING -> mergedSource.value = ResourceState.loading(pagedDataList.value)
            Status.ERROR -> mergedSource.value = ResourceState.error(
                data = pagedDataList.value,
                baseError = fetchState.error ?: BaseError(
                    errorMessage = BaseError.baseErrorMessage
                )
            )
        }
    }
}