package com.bornfight.common.mvvm.basemodels

/**
 * Created by lleopoldovic on 23/09/2019.
 *
 *
 * Status of a resource that is provided to the view (activity/fragment).
 */

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
