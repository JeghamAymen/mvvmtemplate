package com.bornfight.common.mvvm.basemodels

import com.bornfight.common.data.retrofit.ApiInterface

/**
 * Created by lleopoldovic on 25/10/2019.
 */

data class BaseError(
    val errorMessage: String = baseErrorMessage,
    val errorCode: Int = ResponseCodes.UNDEFINED.code,
    val validationErrors: ApiInterface.ValidationErrors = ApiInterface.ValidationErrors()
) {
    companion object {
        var baseErrorMessage: String = "Some error occurred.\nPlease try later."
        var localErrorMessage: String = "Local error occurred."
        var networkErrorMessage: String = "Network error, check your internet connection."
        var emptyErrorBodyMessage: String = "{\"message\":\"Empty error body\"}"
    }
}