package com.bornfight.common.mvvm

import android.util.Log
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.data.retrofit.RetrofitException
import com.bornfight.common.mvvm.basemodels.BaseError
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.basemodels.ResponseCodes
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by lleopoldovic on 16/09/2019.
 */

abstract class BaseRepo(
    private val database: AppDatabase,
    private val api: ApiInterface,
    private val session: SessionPrefImpl,
    private val devicePrefs: DevicePreferences
) {

    init {
        // TODO: Uncomment
        // checkFirebaseSent()
    }

    /**
     * Wraps the fetched data with Observable<ResourceState> for appropriate ViewModel provision.
     *
     * Used for fetching the data both from local and remote source. Provide a @param saveSecondCallData
     * to locally store the remotely fetched data.
     */
    fun <T> twoSideCall(
        firstCall: () -> Observable<T>,
        secondCall: () -> Observable<T>,
        saveSecondCallData: (data: T) -> Unit
    ): Observable<ResourceState<T>> {
        return Observable.mergeDelayError(
            firstCall()
                .map { ResourceState.success(it) }
                .onErrorResumeNext { error: Throwable ->
                    Observable.just(
                        ResourceState.error(
                            handleError(error),
                            null
                        )
                    )
                },
            secondCall()
                .map { ResourceState.success(it) }
                .doAfterNext {
                    it.data?.let { newData -> saveSecondCallData(newData) }
                }
                .onErrorResumeNext { error: Throwable ->
                    Observable.just(
                        ResourceState.error(
                            handleError(error),
                            null
                        )
                    )
                }
                .delay(100, TimeUnit.MILLISECONDS)
        )
    }


    /**
     * Wraps the fetched data with Observable<[ResourceState]> for appropriate ViewModel provision.
     *
     * When fetching the data from an API, provide a @param saveCallData to store the fetched data
     * locally (optionally). Otherwise, use just @param call.
     *
     * Optionally, provide a @param performOnError definition if you would like to perform certain actions
     * in case and when the @param call fails (e.g. provide local data only in case the remote call fails).
     */
    fun <T> oneSideCall(
        call: () -> Observable<T>,
        saveCallData: ((data: T) -> Unit)? = null,
        performOnError: () -> Observable<T> = { Observable.empty() }
    ): Observable<ResourceState<T>> {
        return call()
            .map { ResourceState.success(it) }
            .doAfterNext {
                if (saveCallData != null) {
                    it.data?.let { newData -> saveCallData(newData) }
                }
            }
            .onErrorResumeNext { error: Throwable ->
                Observable.mergeDelayError(
                    performOnError()
                        .map { ResourceState.success(it) }
                        .onErrorResumeNext { subError: Throwable ->
                            Observable.just(
                                ResourceState.error(
                                    handleError(subError),
                                    null
                                )
                            )
                        }
                        .delay(100, TimeUnit.MILLISECONDS),
                    Observable.just(
                        ResourceState.error(
                            handleError(error),
                            null
                        )
                    )
                )
            }
    }

    // TODO: Finish this completable call!
    fun completableCall(
        call: () -> Completable
    ): Observable<ResourceState<Boolean>> {
        return call()
            .toSingleDefault(true)
            .onErrorReturnItem(false)
            .toObservable()
            .map {
                ResourceState.success(it)
            }
            .onErrorResumeNext { error: Throwable ->
                Observable.just(ResourceState.error(handleError(error)))
            }
    }

    /**
     * Processes errors and provides the user with the error message.
     */
    private fun handleError(e: Throwable): BaseError {
        e.printStackTrace()
        if (e is RetrofitException) {
            val errorCode = e.response?.code() ?: ResponseCodes.UNDEFINED.code
            if (e.processNetworkError()) {
                return BaseError(errorMessage = BaseError.networkErrorMessage, errorCode = errorCode)
            }
            if (!e.processNetworkError()) {
                val errorBodyString = e.response?.errorBody()?.string() ?: BaseError.emptyErrorBodyMessage
                e.response?.errorBody()?.close()
                try {
                    val generalError = Gson().fromJson(errorBodyString, ApiInterface.GeneralError::class.java)
                    return BaseError(generalError.message, errorCode)
                } catch (e1: JsonSyntaxException) {
                    Log.i("BasePresenter", "Exception GeneralError: " + e1.message, e1)
                    return try {
                        val errorsList = Gson().fromJson(
                            errorBodyString,
                            object : TypeToken<List<ApiInterface.ValidationErrors.ValidationError>>() {}.type
                        ) as List<ApiInterface.ValidationErrors.ValidationError>
                        val validationErrors =
                            ApiInterface.ValidationErrors().apply { setErrors(errorsList) }
                        BaseError(validationErrors.getErrors(), errorCode, validationErrors)
                    } catch (e2: JsonSyntaxException) {
                        Log.e("BasePresenter", "Exception ValidationError: " + e2.message, e2)
                        try {
                            Crashlytics.log(Log.WARN, "Server Response Error", errorBodyString)
                        } catch (e: IllegalStateException) {
                            /* CL not initialized, ignore */
                        }
                        BaseError(errorMessage = BaseError.baseErrorMessage, errorCode = errorCode)
                    }
                }
            }
        } else {
            return BaseError(errorMessage = BaseError.localErrorMessage)
        }

        return BaseError(errorMessage = BaseError.baseErrorMessage)
    }

    private fun checkFirebaseSent() {
        if (session.isLoggedIn && !devicePrefs.isFirebaseTokenSent) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                api.saveFirebaseId(it.token, "android")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : DisposableSingleObserver<Any>() {
                        override fun onSuccess(any: Any) {
                            devicePrefs.isFirebaseTokenSent = true
                        }

                        override fun onError(e: Throwable) {

                        }
                    })
            }
        }
    }
}