package com.bornfight.common.data.retrofit

import android.text.TextUtils
import com.bornfight.appname.BuildConfig
import com.bornfight.demo.weather.model.ForecastResponse
import com.bornfight.demo.weather.model.NewsResponse
import com.bornfight.demo.weather.model.WeatherResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*
import java.util.*

/**
 * Created by tomislav on 26/02/16.
 */

interface ApiInterface {

    //API calls
    // TODO remove demo calls when not needed anymore
    @GET("weather")
    fun getWeatherForCity(@Query("q") city: String): Observable<WeatherResponse>

    @GET("forecast")
    fun getForecastForCity(@Query("q") city: String): Observable<ForecastResponse>


    @GET(BuildConfig.BASE_NEWS_API_URL + "everything?&apiKey=162aa2e23d074514bbe96eeddca6c9e1")
    fun getNews(
        @Query("page") pageNo: Int,
        @Query("pageSize") pageSize: Int,
        @Query("q") query: String
    ): Observable<NewsResponse>


    //firebase
    @POST("firebases")
    @FormUrlEncoded
    fun saveFirebaseId(@Field("registration_id") registrationId: String, @Field("platform") platform: String): Single<Any>

    @FormUrlEncoded
    @HTTP(
        method = "DELETE",
        path = BuildConfig.BASE_API_URL + "firebases/reg-delete",
        hasBody = true
    )
    fun deleteFirebaseId(@Field("registration_id") registrationId: String): Single<Any>

    //Errors
    class GeneralError {
        @Expose
        @SerializedName("error", alternate = ["message"])
        var message: String = ""
        @Expose
        @SerializedName("errors")
        var errors: List<String> = emptyList()
    }

    class ValidationErrors {
        @Expose
        @SerializedName("errors")
        internal var errors: List<ValidationError> = ArrayList()

        fun getFieldErrors(fieldName: String): List<String> {
            for (ve in errors) {
                if (ve.field == fieldName) {
                    val messages = ve.messages?.toList()
                    return if (messages?.isNotEmpty() == true) {
                        messages
                    } else {
                        listOf(ve.message)
                    }
                }
            }
            return emptyList()
        }

        fun hasErrors(): Boolean {
            return errors.isNotEmpty()
        }

        fun getFieldError(fieldName: String, listDelimiter: CharSequence): String {
            for (ve in errors) {
                if (ve.field == fieldName) {
                    return if (ve.messages?.isNotEmpty() == true) {
                        TextUtils.join(listDelimiter, ve.messages ?: emptyList<String>())
                    } else {
                        ve.message
                    }
                }
            }
            return ""
        }

        fun getErrors(): String {
            val stringBuilder = StringBuilder()
            for (ve in errors) {
                if (ve.messages?.isNotEmpty() == true) {
                    stringBuilder.append(TextUtils.join(", ", ve.messages ?: emptyList<String>())).append("\n")
                } else {
                    stringBuilder.append(ve.message).append("\n")
                }
            }
            return stringBuilder.toString()
        }

        fun setErrors(errors: List<ValidationError>) {
            this.errors = errors
        }

        inner class ValidationError {
            @SerializedName("field")
            internal var field = ""
            @SerializedName("messages")
            internal var messages: List<String>? = emptyList()
            @SerializedName("message")
            internal var message = ""
        }
    }


}


