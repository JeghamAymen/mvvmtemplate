package com.bornfight.appname

import android.app.Application
import com.bornfight.common.dagger.components.ApplicationComponent
import com.bornfight.common.dagger.components.DaggerApplicationComponent
import com.bornfight.common.dagger.modules.ContextModule
import com.bornfight.common.firebase.NotificationChannelsHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 * Created by lleopoldovic on 13/09/2019.
 * TODO delete demo.weather package when not needed anymore
 */

class App : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    lateinit var component: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        DaggerApplicationComponent.builder()
            .contextModule(ContextModule(this))
            .build()
            .also { component = it }
            .inject(this)

        Glide.with(this).setDefaultRequestOptions(
            RequestOptions().diskCacheStrategy(
                DiskCacheStrategy.AUTOMATIC
            )
        )

        NotificationChannelsHelper.initChannels(this)
    }
}