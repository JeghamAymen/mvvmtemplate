package com.bornfight.demo.weather.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

data class Weather(
    @SerializedName("id")
    val id: Int,
    @SerializedName("main")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String
)

class WeatherConverter {

    val gson = Gson()

    @TypeConverter
    fun fromWeatherList(weather: List<Weather>): String {
        return gson.toJson(weather)
    }

    @TypeConverter
    fun toWeatherList(weather: String?): List<Weather> {
        if (weather.isNullOrBlank()) {
            return listOf()
        }

        val listType = object : TypeToken<List<Weather>>() {}.type

        return gson.fromJson(weather, listType)
    }
}