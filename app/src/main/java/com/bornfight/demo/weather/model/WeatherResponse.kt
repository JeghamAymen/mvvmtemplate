package com.bornfight.demo.weather.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

@Entity
@TypeConverters(WeatherConverter::class)
data class WeatherResponse(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("weather")
    val weather: List<Weather>,
    @Embedded
    @SerializedName("main")
    val main: Main,
    @SerializedName("name")
    val name: String,
    @SerializedName("dt")
    val date: Long
) {
    fun getImageUrl(): String {
        return "https://openweathermap.org/img/w/${weather[0].icon}.png"
    }

    fun getDateTimeString(): String {
        val format = SimpleDateFormat("dd.MM. HH:mm", Locale.US)
        return format.format(date * 1000)
    }
}