package com.bornfight.demo.weather.news

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.pagination.BasePaginationViewModel
import com.bornfight.common.mvvm.pagination.GenericPaginationDataSourceFactory
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.demo.weather.model.News
import com.bornfight.demo.weather.model.pagination.NewsPaginationParams
import com.bornfight.demo.weather.repository.news.NewsRepo
import javax.inject.Inject

class NewsViewModel @Inject constructor(
    schedulers: SchedulerProvider,
    private val newsRepo: NewsRepo
) : BasePaginationViewModel(schedulers) {

    // Lateinits
    private lateinit var newsDataSourceFactory: GenericPaginationDataSourceFactory<News, NewsPaginationParams>

    // LiveDatas
    lateinit var newsList: LiveData<ResourceState<PagedList<News>>>

    // Other
    private var paginationParams: NewsPaginationParams = NewsPaginationParams("").apply {
        pageNumber = 1
        loadBefore = false
    }

    init {
        newsDataSourceFactory = generatePaginationDataSourceFactory(
            paginationParams = paginationParams,
            call = { newsRepo.getNewss(it as NewsPaginationParams) }
        )
        val newsPagedListConfig = generateDefaultPaginationConfig(
            paginationParams = paginationParams
        )

        newsList = buildLivePagedListWithState(newsDataSourceFactory, newsPagedListConfig)
    }

    fun search(query: String) {
        newsDataSourceFactory.reInit(paginationParams.apply {
            this.query = query; this.pageNumber = 1
        })
    }
}