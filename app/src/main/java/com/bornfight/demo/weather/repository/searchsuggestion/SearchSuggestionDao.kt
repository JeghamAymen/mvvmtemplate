package com.bornfight.demo.weather.repository.searchsuggestion

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bornfight.demo.weather.model.SearchSuggestion
import io.reactivex.Observable

/**
 * Created by lleopoldovic on 13/09/2019.
 */

@Dao
interface SearchSuggestionDao : SearchSuggestionData {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun saveSearchSuggestion(searchSuggestion: SearchSuggestion)

    @Query("SELECT * FROM searchsuggestion ORDER BY id DESC LIMIT 10")
    override fun getSearchSuggestions(): Observable<List<SearchSuggestion>>
}