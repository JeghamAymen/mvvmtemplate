package com.bornfight.demo.weather.repository.news

import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvvm.BaseRepo
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.demo.weather.model.News
import com.bornfight.demo.weather.model.pagination.NewsPaginationParams
import io.reactivex.Observable
import javax.inject.Inject

class NewsRepoImpl @Inject constructor(
    private val database: AppDatabase,
    private val api: ApiInterface,
    session: SessionPrefImpl,
    preferences: DevicePreferences
): BaseRepo(database, api, session, preferences), NewsRepo {

    override fun getNewss(params: NewsPaginationParams): Observable<ResourceState<List<News>>> {
        return oneSideCall({
            api.getNews(params.pageNumber, params.pageSize, params.query).map { it.articles }
        }, {
            saveNewss(it)
        }, {
            database.newsDao().getNewss(params.pageSize, params.pageSize * (params.pageNumber - 1))
        })
    }

    override fun saveNewss(items: List<News>) {
        database.newsDao().saveNewss(items)
    }

    override fun deleteNewss(items: List<News>) {
        database.newsDao().deleteNewss(items)
    }
}