package com.bornfight.demo.weather.searchsuggestions.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bornfight.appname.R
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.utils.adapters.GenericAdapter
import kotlinx.android.synthetic.main.search_suggestion_item.view.*

class SearchSuggestionsAdapter : GenericAdapter<SearchSuggestion>() {
    override fun getLayoutId(viewType: Int): Int {
        return R.layout.search_suggestion_item
    }

    override fun getViewHolder(view: View, viewType: Int): GenericViewHolder<SearchSuggestion> {
        return SearchSuggestionViewHolder(view).apply {
            this.itemView.setOnClickListener {
                if (adapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener

                // TODO
            }
        }
    }

    inner class SearchSuggestionViewHolder(itemView: View) :
        GenericAdapter.GenericViewHolder<SearchSuggestion>(itemView) {
        override fun bind(data: SearchSuggestion) {
            itemView.searchSuggestionTV.text = data.suggestion
        }
    }
}