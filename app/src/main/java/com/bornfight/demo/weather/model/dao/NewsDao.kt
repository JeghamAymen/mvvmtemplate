package com.bornfight.demo.weather.model.dao

import androidx.room.*
import com.bornfight.demo.weather.model.News
import io.reactivex.Observable

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNewss(items: List<News>)

    @Update
    fun updateNewss(items: List<News>)

    @Query("SELECT * FROM News ORDER BY title ASC LIMIT :take OFFSET :skip")
    fun getNewss(take: Int, skip: Int): Observable<List<News>>

    @Delete
    fun deleteNewss(items: List<News>)

    @Query("SELECT * FROM News WHERE description LIKE :query ORDER BY title ASC LIMIT :pageSize OFFSET :pageNumber")
    fun fetchNews(pageNumber: Int, pageSize: Int, query: String): Observable<List<News>>
}