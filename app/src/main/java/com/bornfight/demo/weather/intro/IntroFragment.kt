package com.bornfight.demo.weather.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.bornfight.appname.R
import com.bornfight.common.mvvm.BaseFragment
import kotlinx.android.synthetic.main.fragment_intro.*

// TODO delete demo.weather package when not needed anymore

class IntroFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goToWeatherButton.setOnClickListener {
            findNavController().navigate(IntroFragmentDirections.actionIntroToNews())
        }

        goToNewsButton.setOnClickListener {
            findNavController().navigate(IntroFragmentDirections.actionIntroToWeather())
        }
    }
}