package com.bornfight.demo.weather.news.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bornfight.appname.R
import com.bornfight.common.kotlin.loadUrl
import com.bornfight.demo.weather.model.News
import kotlinx.android.synthetic.main.item_news.view.*

class NewsListAdapter() : PagedListAdapter<News, RecyclerView.ViewHolder>(NewsDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NewsViewHolder).bind(getItem(position))
    }

    companion object {
        val NewsDiffCallback = object : DiffUtil.ItemCallback<News>() {
            override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class NewsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(news: News?) {
            news?.let {
                with(itemView) {
                    txt_news_name.text = news.title
                    txt_news_number.text = (adapterPosition + 1).toString()
                    news.image?.let { img_news_banner.loadUrl(it) }
                }
            }
        }
    }
}