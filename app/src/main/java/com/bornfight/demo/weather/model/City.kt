package com.bornfight.demo.weather.model

import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("id")
    val cityId: Int,
    @SerializedName("name")
    val name: String
)