package com.bornfight.demo.weather.repository.weather

import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import io.reactivex.Observable

interface WeatherData {

    fun getWeatherData(city: String): Observable<WeatherResponse>

    fun getForecastData(city: String): Observable<List<Forecast>>

    fun saveWeatherData(weather: WeatherResponse)

    fun saveForecastData(forecast: List<Forecast>)

    fun clearForecastTable()

}