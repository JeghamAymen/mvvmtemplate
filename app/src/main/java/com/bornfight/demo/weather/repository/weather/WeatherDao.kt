package com.bornfight.demo.weather.repository.weather

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import io.reactivex.Observable

@Dao
interface WeatherDao : WeatherData {

    @Query("SELECT * FROM WeatherResponse WHERE name = :city")
    override fun getWeatherData(city: String): Observable<WeatherResponse>

    @Query("SELECT * FROM Forecast WHERE name = :city")
    override fun getForecastData(city: String): Observable<List<Forecast>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun saveWeatherData(weather: WeatherResponse)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun saveForecastData(forecast: List<Forecast>)

    @Query("DELETE FROM Forecast")
    override fun clearForecastTable()
}