package com.bornfight.demo.weather.repository.weather

import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import io.reactivex.Observable

interface WeatherRepo {

    fun getCurrentWeatherForCity(cityName: String): Observable<ResourceState<WeatherResponse>>

    fun getForecastForCity(cityName: String): Observable<ResourceState<List<Forecast>>>

}