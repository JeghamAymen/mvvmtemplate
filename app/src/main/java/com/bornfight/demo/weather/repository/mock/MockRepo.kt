package com.bornfight.demo.weather.repository.mock

import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionData
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by lleopoldovic on 03/10/2019.
 */

class MockRepo @Inject constructor(
    private val database: AppDatabase,
    private val api: ApiInterface,
    private val session: SessionPrefImpl
) : SearchSuggestionData {

    override fun getSearchSuggestions(): Observable<List<SearchSuggestion>> {
        return Observable.just(
            listOf(
                SearchSuggestion(0, "Zagreb"),
                SearchSuggestion(1, "Berlin"),
                SearchSuggestion(2, "London")
            )
        )
    }

    override fun saveSearchSuggestion(searchSuggestion: SearchSuggestion) {}
}