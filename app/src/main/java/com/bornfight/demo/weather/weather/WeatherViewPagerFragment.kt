package com.bornfight.demo.weather.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.bornfight.appname.R
import com.bornfight.common.mvvm.BaseFragment
import com.bornfight.demo.weather.weather.adapters.WeatherViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_weather_viewpager.*

class WeatherViewPagerFragment : BaseFragment() {

    private val viewPagerAdapter: WeatherViewPagerAdapter by lazy {
        WeatherViewPagerAdapter(
            childFragmentManager, viewLifecycleOwner.lifecycle, arguments?.getString(
                ARG_CITY, "Zagreb"
            ) ?: "Zagreb"
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_weather_viewpager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        weatherFragmentVP.adapter = viewPagerAdapter
        viewPagerAdapter.setItemList(
            listOf(
                System.currentTimeMillis(),
                System.currentTimeMillis() + 24 * 60 * 60 * 1000,
                System.currentTimeMillis() + 2 * 24 * 60 * 60 * 1000
            )
        )
        weatherFragmentVP.orientation = ViewPager2.ORIENTATION_VERTICAL
    }

    companion object {

        const val ARG_CITY = "city"

        @JvmStatic
        fun newInstance(cityName: String) =
            WeatherViewPagerFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CITY, cityName)
                }
            }
    }

}