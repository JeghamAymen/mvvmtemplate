package com.bornfight.demo.weather.model.pagination

import com.bornfight.common.mvvm.pagination.GenericPaginationParams

data class NewsPaginationParams(
    var query: String
): GenericPaginationParams<NewsPaginationParams>()