package com.bornfight.demo.weather.repository.searchsuggestion

import com.bornfight.demo.weather.model.SearchSuggestion
import io.reactivex.Observable

/**
 * Created by lleopoldovic on 13/09/2019.
 */

interface SearchSuggestionData {

    fun saveSearchSuggestion(searchSuggestion: SearchSuggestion)

    fun getSearchSuggestions(): Observable<List<SearchSuggestion>>
}