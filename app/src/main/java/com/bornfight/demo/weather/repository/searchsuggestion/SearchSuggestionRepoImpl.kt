package com.bornfight.demo.weather.repository.searchsuggestion

import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvvm.BaseRepo
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.demo.weather.model.SearchSuggestion
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by lleopoldovic on 13/09/2019.
 */

// Check the following links to get a bit more familiar with combining Room with RxJava2:
// https://developer.android.com/topic/libraries/architecture/livedata#use_livedata_with_room
// https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757

class SearchSuggestionRepoImpl @Inject constructor(
    private val database: AppDatabase,
    private val api: ApiInterface,
    private val session: SessionPrefImpl,
    private val devicePrefs: DevicePreferences
) : BaseRepo(database, api, session, devicePrefs), SearchSuggestionRepo {

    override fun getSearchSuggestions(): Observable<ResourceState<List<SearchSuggestion>>> {
        return oneSideCall({
            database.searchSuggestionDao().getSearchSuggestions()
        })
    }

    override fun saveSearchSuggestion(searchSuggestion: SearchSuggestion) {
        database.searchSuggestionDao().saveSearchSuggestion(searchSuggestion)
    }
}