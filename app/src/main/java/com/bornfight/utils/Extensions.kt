package com.bornfight.utils

fun Double.kelvinToCelsiusString(): String {
    return "${String.format("%.2f", this - 273.15)}°C"
}