package com.bornfight.utils

import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

/**
 * Use Logger to log all unexpected call to Firebase.
 */
object Logger {

    fun log(message: String) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message)
        }
    }

    fun logException(exception: Exception) {
        if (Fabric.isInitialized()) {
            Crashlytics.logException(exception)
        }
    }

    fun log(priority: Int, tag: String, msg: String) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(priority, tag, msg)
        }
    }
}