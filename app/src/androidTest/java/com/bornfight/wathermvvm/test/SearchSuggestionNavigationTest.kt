package com.bornfight.wathermvvm.test

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.bornfight.appname.R
import com.bornfight.demo.weather.searchsuggestions.SearchSuggestionsFragment
import com.bornfight.demo.weather.searchsuggestions.SearchSuggestionsFragmentDirections
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode

@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
class SearchSuggestionNavigationTest {

    lateinit var scenario: FragmentScenario<SearchSuggestionsFragment>

    @Mock
    lateinit var navController: NavController

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        scenario = launchFragmentInContainer<SearchSuggestionsFragment>().apply {
            onFragment {
                Navigation.setViewNavController(it.view!!, navController)
            }
        }
    }

    @Test
    fun clickAdd_goToWeatherFragment() {
        Espresso.onView(ViewMatchers.withId(R.id.searchET)).perform(ViewActions.replaceText("Zagreb"))
        Espresso.onView(ViewMatchers.withId(R.id.addSearchSuggestionBtn)).perform(ViewActions.click())

        Thread.sleep(2000)

        Mockito.verify(navController).navigate(
            SearchSuggestionsFragmentDirections.actionSearchSuggestionsToWeatherViewPager2("Zagreb")
        )
    }

    @Test
    fun clickAdd_stayAtSearchSuggestionsScreen() {

        Espresso.onView(ViewMatchers.withId(R.id.addSearchSuggestionBtn)).perform(ViewActions.click())

        Mockito.verify(
            navController,
            Mockito.never()
        ).navigate(SearchSuggestionsFragmentDirections.actionSearchSuggestionsToWeatherViewPager2(""))
    }

}